drop database if exists testdb;

create database testdb;

GRANT ALL PRIVILEGES ON testdb.* TO www@"%" IDENTIFIED BY 'my_password' WITH GRANT OPTION;

use testdb;

CREATE TABLE `comments` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `text` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;


insert into comments (text) values ("Комментарий1");
insert into comments (text) values ("Комментарий2");

