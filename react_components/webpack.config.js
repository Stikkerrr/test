var HTMLWebpackPlugin = require('html-webpack-plugin');
var HTMLWebpackPluginConfig = new HTMLWebpackPlugin({
	template: __dirname + '/app/index.html',
	filename: 'index.html',
	inject: 'head',
	minify: false,
	hash: true,
});

module.exports = {
	entry: __dirname + '/app/index.js',
	module: {
		rules: [
			{
				test: /\.js$/,
			    exclude: /(node_modules|bower_components)/,
				use: [
					{
						loader: 'babel-loader',
						options: {
							presets: ['@babel/preset-react'],
						},
					}
				],
			},
			{
				test: /\.css$/,
                use: ['style-loader', 'css-loader'],

			},

		],

	},
	output: {
		filename: 'react_components' + '.js',
//		chunkFilename: 'vendors.js',
		path: __dirname + '/build',
		library: ['myComponents'],
	},

//	optimization: {
//        splitChunks: {
//            chunks: 'all'
//        }
//    },


	plugins: [HTMLWebpackPluginConfig],
	devServer: {
		port: 8099,
        https: false,
		public: "127.0.0.1:8099",
		host: '0.0.0.0'
	},
};
